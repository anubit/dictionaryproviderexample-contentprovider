This application is an example from Udacity content provider example

### What is this repository for? ###

* It uses UserDictionary content provider provided by Android.
* This diplays the word and its frequency that we are querying from the dictionary.
* It should work on API 23. If it does not display the list view, use API 22 in emulator

#### Set up ####
Make sure you have imported android.support.v4.widget.SimpleCursorAdapter for SimpleCursorAdapter

* API 22
* Add data to dictionary via ContentValues and insert via ContentResolver()
* Make sure you have inserted the data in the dictionary before you run.