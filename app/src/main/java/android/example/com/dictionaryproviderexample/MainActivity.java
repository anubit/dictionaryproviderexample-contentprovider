/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.example.com.dictionaryproviderexample;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.provider.UserDictionary.Words;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ListView;

/**
 * This is the central activity for the Provider Dictionary Example App. The purpose of this app is
 * to show an example of accessing the {@link Words} list via its' Content Provider.
 */
public class MainActivity extends ActionBarActivity {

    // For the SimpleCursorAdapter to match the UserDictionary columns to layout items.
    private static final String[] COLUMNS_TO_BE_BOUND  = new String[] {
            UserDictionary.Words.WORD,
            UserDictionary.Words.FREQUENCY
    };

    // android.two_line_list_item
    private static final int[] LAYOUT_ITEMS_TO_FILL = new int[] {
            android.R.id.text1,
            android.R.id.text2
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //changing from textview to list view  will be populated with the Dictionary ContentProvider data.
        ListView dictListView = (ListView) findViewById(R.id.dictionary_list_view);

        // manually add some values in the dictionary:
        ContentValues newValues = new ContentValues();

        newValues.put(UserDictionary.Words.APP_ID, "my_app_id");
        newValues.put(UserDictionary.Words.WORD, "test");
        newValues.put(UserDictionary.Words.FREQUENCY, "10");

        // Get the ContentResolver which will send a message to the ContentProvider
        ContentResolver resolver = getContentResolver();

        // insert the content values in the dictionary
        resolver.insert(
                UserDictionary.Words.CONTENT_URI,
                newValues);
        // Get a Cursor containing all of the rows in the Words table
        // (Uri uri,
      /*  String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder)*/
        // here we are selecting all rows and cols
        Cursor cursor = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, null);

        Log.d("TEST", "content uri = " + UserDictionary.Words.CONTENT_URI);

        // Surround the cursor in a try statement so that the finally block will eventually execute

        // SimpleCursorAdapter(context,layout,cursor,
        // columns in the cursor to use stored in string array,
        // ids. of textview contained in the layout that you passed int the 1st parameter stored
        // as an int array.

        Log.d("dictionary", " word = " + UserDictionary.Words.WORD);
            SimpleCursorAdapter simpleCursorAdapter =
                    new SimpleCursorAdapter(this, android.R.layout.two_line_list_item, cursor,
                            COLUMNS_TO_BE_BOUND, LAYOUT_ITEMS_TO_FILL, 0);
            dictListView.setAdapter(simpleCursorAdapter);




        }


}
